import Actions from './Actions/Actions';
import './Header.css';
import Menu from './Menu/Menu';
import Search from './Search/Search';

function Header() {
    return (
      <header className="Header">
        <div className='Header__wrapper'>
            <div className='Header__container_container'>
                <div className='Header__body'>

                    <div className='Header__main'>
                        <Menu />
                    </div>

                    <div className='Header__actions'>
                        <Actions />
                    </div>


                </div>
            </div>
        </div>
      </header>
    );
  }
  
  export default Header;