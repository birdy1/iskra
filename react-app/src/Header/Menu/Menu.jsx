import './Menu.css';
import ReactDOM from "react-dom";
import { useState } from 'react';

function Menu() {

    let [nav, setNav] = useState(false);
    let [nav_burg, setNav_burg] = useState(false);
    let [nav_burg_2, setNav_burg_2] = useState(false);
    let [nav_burg_3, setNav_burg_3] = useState(false);

    
    return (
        <> 

        <button className='menu_btn_burger' onClick={() => setNav(!nav)}></button>
        <a href='' className='Header__logo'>ИСКРА</a> 

            <div className='Header__menu'>
                <nav className={`Menu__body ${nav ? 'active' : ''}`}>
                    <ul className='Menu__list'>

                        <li className='Menu__item'>
                            <a className='Menu__link'>Мотопилы</a>
                            <button onClick={() => setNav_burg(!nav_burg)} className={`menu_arrow icon_arrow ${nav_burg ? 'arrow_active' : ''}`}></button>
                            <ul className={`menu_sub-list ${nav_burg ? 'menu_sub-list_active' : ''}`}>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#1" className='menu__sub-link'>Бензопилы Stihl</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#10" className='menu__sub-link'>Электропилы Stihl</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#12" className='menu__sub-link'>Бензопилы Champion</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#18" className='menu__sub-link'>Электропилы Champion</a>
                                </li>
                            </ul>
                        </li>

                        <li className='Menu__item'>
                            <a className='Menu__link'>Мотокосы</a>
                            <button onClick={() => setNav_burg_2(!nav_burg_2)} className={`menu_arrow icon_arrow ${nav_burg_2 ? 'arrow_active' : ''}`}></button>
                            <ul className={`menu_sub-list ${nav_burg_2 ? 'menu_sub-list_active' : ''}`}>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#1.2" className='menu__sub-link'>Триммеры Stihl</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#4.2" className='menu__sub-link'>Мотокосы Stihl</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#6.2" className='menu__sub-link'>Триммеры Champion</a>
                                </li>
                            </ul>
                        </li>

                        <li className='Menu__item'>
                            <a className='Menu__link'>Разное</a>
                            <button onClick={() => setNav_burg_3(!nav_burg_3)} className={`menu_arrow icon_arrow ${nav_burg_3 ? 'arrow_active' : ''}`}></button>
                            <ul className={`menu_sub-list ${nav_burg_3 ? 'menu_sub-list_active' : ''}`}>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#1.3" className='menu__sub-link'>Газонокосилки Stihl</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#12.3" className='menu__sub-link'>Генераторы Champion</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#17.3" className='menu__sub-link'>Мотокультиваторы Champion</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#21.3" className='menu__sub-link'>Снегоотбрасыватели Champion</a>
                                </li>
                                <li className='menu_sub-item'>
                                    <a onClick={() => setNav(!nav)} href="#32.3" className='menu__sub-link'>Мойки Champion</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </>
    )
};

export default Menu;