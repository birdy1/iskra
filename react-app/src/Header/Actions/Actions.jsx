import { useCallback, useState } from 'react';
import './Actions.css';
import { useSelector } from 'react-redux';
import Cart from './cart/cart';



function Actions() {
    let [cartOpen, setCartOpen] = useState(false);

    const count = useSelector((store) => store.cart.products.length);

    const cart = useSelector((store) => store.cart.products);




    return (
        <div className='Header__actions actions-header'>
            <div className='actions-header__item cart-header'>
                <button onClick={() => setCartOpen(cartOpen = !cartOpen)} className={`_icon-cart ${cartOpen ? '_icon-cart_active' : ''}`}></button><span className='cart-header__span'> {count} </span>
                
                {cartOpen && (  

                <div className='cart-header__body'>

                    <h4 className='cart-header__list cart-list'>Количество товаров в корзине: {count}</h4>

                    {cart.map(item => <Cart id={item.id} title={item.title} price={item.price} img={item.img} />)}

                </div>  
                )
                }

            </div>
        </div>
    )
};

export default Actions;