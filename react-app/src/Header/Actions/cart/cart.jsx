import React from 'react';
import './cart.css';


function cart(props) {


    return (
      <div className='cart__container'>
        <div className='cart__cart'>

            <img className='cart__img' src={props.img}></img>

              <div className='cart__priceandtitle'>
                  <h4 className="cart__title">{props.title}</h4>
                  <div className='cart__price'>{props.price} руб.</div>
              </div>

        </div>
      </div>
    )
  };



export default cart;