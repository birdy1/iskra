import './Footer.css';


function Footer() {
    return (
        <footer className='footer'>
            <div className='footer__container _container'>
                <div className="footer__body">

                    <div className="footer__main">
                        <a href='' className='footer__logo'>ИСКРА</a>
                        <div className="footer__text"> «ИСКРА» представляет специализированный магазин Stihl – один из лучших в Ирбите и Свердловской области.
                         В продаже любая техника Stihl, Viking и Champion, все аксессуары и расходные материалы.</div>
                        <div className="footer_contacts contacts-footer"></div>
                        <div className='footer_contact_phoneandlocation'><div className='_icon-location'></div><a target='_blank' href='https://www.google.com/maps/place/%D0%A1%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D0%BD%D1%8B%D0%B9+%D1%86%D0%B5%D0%BD%D1%82%D1%80+%D0%98%D0%A1%D0%9A%D0%A0%D0%90%26STIHL/@57.6814433,63.0539943,19z/data=!4m6!3m5!1s0x43bf084269bb8685:0x1e4423fb80c41bc6!8m2!3d57.6815335!4d63.0539351!16s%2Fg%2F11f4_bg_rh' className='contacts-footer__item'>Красноармейская ул., 11, Ирбит, Свердловская обл., 623850</a></div>
                        <div className='footer_contact_phoneandlocation'><div className='_icon-phone'></div><a href="tel:+79505558581" className='contacts-footer__item'>+7 (950) 555-85-81</a></div>
                        <a href='' target='_blank' className='contacts-footer__item'>Здесь ссылка</a>
                    </div>

                    <div data-spollers="768,max" className='footer__menu menu-footer'>
                        <div className='menu-footer__column'>
                            <button type='button' data-spoller className='menu-footer__title _footer-tittle'>МЕНЮ</button>
                            <ul className='menu-footer__list'>
                                <li><a href='' className='menu-footer__link'>Главная</a></li>
                                <li><a href='' className='menu-footer__link'>Мотопилы</a></li>
                                <li><a href='' className='menu-footer__link'>Мотокосы</a></li>
                                <li><a href='' className='menu-footer__link'>Газонокосилки</a></li>
                            </ul>
                        </div>
                    </div>

            
                </div>
            </div>
        </footer>
    )
};

export default Footer;