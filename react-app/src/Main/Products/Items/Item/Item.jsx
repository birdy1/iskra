import React, { Component } from "react";
import { useDispatch } from "react-redux";
import './Item.css'
import { addProduct } from "../../../../reducers/cart-reducer";
import { removeProducts } from "../../../../reducers/cart-reducer";
import { useCallback, useState } from 'react';

 function Item(props) {

    const dispatch = useDispatch();

    function handleAddProduct() {
        const action = addProduct(props);
        dispatch(action);

        setAddCart(cart = !cart)
    }


    function handleRemoveProducts() {
        const action = removeProducts(props);
        dispatch(action);

        setAddCart(cart = !cart)
    }
    

    let [cart, setAddCart] = useState(false);
    

        return (
        <>

                        <div id={props.id} className='products__item item-product'>
                            <a className='item-products__image'>
                                <img className="item_image" src={props.img} ></img>
                            </a>

                            <div className='item-products__body'>
                                <div className='item-products__content'>
                                    <h3 className='item-products__tittle'>{props.title}</h3>
                                <div className='item-products__text'></div>
                            </div>

                            <div className='item-products__prices'>
                                <div className='item-products__price'>Руб. {props.price}</div>
                                    <div className='item-products__price item-products__price__old'></div>
                                </div>
                            </div>


                            {!cart && ( 
                            <div className='item-product__actions actions-products'>
                                <div className='actions-product__body'>
                                    <button onClick={handleAddProduct} className={'actions-products_button btn'}>Добавить в корзину</button>
                                </div>
                            </div>
                            )}

                            {cart && ( 
                            <div className='item-product__actions actions-products'>
                                <div className='actions-product__body'>
                                    <button onClick={handleRemoveProducts} className={'actions-products_button_active btn'}>Добавлено</button>
                                </div>
                            </div>
                            )}



                        </div>
                        


            </>
        )
    }


export default Item;