import React, { Component } from "react";
import './Items.css'
import Item from './Item/Item';
import { useSelector } from 'react-redux';

function Items() {

const chainsaws = useSelector((store) => store.cart.chainsaws);
const trimmers = useSelector((store) => store.cart.trimmers);
const other = useSelector((store) => store.cart.other);


        return (
        <div className='products__container'>
            <h2 className='products__tittle'>Мотопилы</h2>


                    <div className='products__items'> 
                        {chainsaws.map(item => <Item id={item.id} title={item.title} price={item.price} img={item.img} />)}
                    </div>


            <h2 className='products__tittle'>Мотокосы</h2>


                    <div className='products__items'> 
                        {trimmers.map(item => <Item id={item.id} title={item.title} price={item.price} img={item.img} />)}
                    </div>


            
            <h2 className='products__tittle'>Разное</h2>


                    <div className='products__items'> 
                        {other.map(item => <Item id={item.id} title={item.title} price={item.price} img={item.img} />)}
                    </div>



        </div>
        )

}

export default Items;