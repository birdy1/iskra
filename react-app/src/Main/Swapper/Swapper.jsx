import './Swapper.css';

import slider1 from "../../img/swapper_banner_sliders/slider1.jpg";
import slider2 from "../../img/swapper_banner_sliders/slider2.jpg";
import slider3 from "../../img/swapper_banner_sliders/slider3.jpeg";


import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";

function Swapper () {
  return (
    <>
    <Swiper
      pagination={{
        dynamicBullets: true,
      }}
      modules={[Pagination]}
      className="mySwiper"
    >
      <SwiperSlide><img src={slider1} /></SwiperSlide>
      <SwiperSlide><img src={slider2} /></SwiperSlide>
      <SwiperSlide><img src={slider3} /></SwiperSlide>
      
    </Swiper>
  </>
  );
};

export default Swapper;

