import './Main.css';
import Advantages from './Advantages/Advantages';
import Swapper from './Swapper/Swapper';
import Products from './Products/Products';

function Main() {
    return (
        <div className='wrapper'>
            <main className='page'>
                <section className='page__main-slider main-sloder'>
                    <div className='main-slaider__container'>
                        <div className="main-slider__body">

                            <div className="main-slider__content contant-main">
                                <h1 className='content-main__tittle'>Гарантия Высококачественного Ремонта</h1>
                                <div className='content-main_text'>
                                Cобственный сервисный центр по обслуживанию и ремонту всех инструментов Stihl и других брендов.
                                </div>
                                <a href="tel:+79505558581" className='content-main_button btn'>Связаться с нами</a>
                            </div>

                                <Swapper />            
                                <Advantages />
                                <Products />


                        </div>
                    </div>
                </section>
            </main>
        </div>
    )
};

export default Main;