import './Advantages.css';
import Advantages01 from "../../img/advantages/01.svg";
import Advantages02 from "../../img/advantages/02.svg";
import Advantages03 from "../../img/advantages/03.svg";


function Advantages() {
    return (
        <div className='Advantages'>
            <div className='Advantages__container'>

                <div className='Advantages__item'>
                <img src={Advantages01} className='Advantages__icon'></img>
                    <div className='Advantages__promo'>
                        <div className='Advantages__tittle'>Высокое качество</div> 
                        <div className='Advantages__text'>Вся техника сертифицирована</div>
                    </div>
                </div>

                <div className='Advantages__item'>
                    <img src={Advantages02} className='Advantages__icon'></img>
                    <div className='Advantages__promo'>
                        <div className='Advantages__tittle'>Гарантия</div> 
                        <div className='Advantages__text'>Минимум 1 год</div>
                    </div>
                </div>

                <div className='Advantages__item'>
                <img src={Advantages03} className='Advantages__icon'></img>
                    <div className='Advantages__promo'>
                        <div className='Advantages__tittle'>Собственный сервисный центр</div> 
                        <div className='Advantages__text'>С лучшими мастерами своего дела</div>
                    </div>
                </div>

            </div>
        </div>
    )
};

export default Advantages;